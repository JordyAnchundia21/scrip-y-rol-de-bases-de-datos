CREATE ROLE ControlInscripcion;

GRANT SELECT,INSERT,UPDATE,DELETE on alumno,materia TO ControlInscripcion;

CREATE USER Profesor IN ROLE ControlInscripcion PASSWORD '123456';	