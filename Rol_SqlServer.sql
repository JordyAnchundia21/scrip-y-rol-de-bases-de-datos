/*==============================================================*/
/* CREACION DE USUARIOS Y ROLES                                 */
/*==============================================================*/



create login profesor with password='123456';

create user userprofesor for login profesor;

create role rol_controlInscripcion;

alter role rol_controlInscripcion add member[userprofesor];

Grant select, insert, update, delete to rol_controlInscripcion;
