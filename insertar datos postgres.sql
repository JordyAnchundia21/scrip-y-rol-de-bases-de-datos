insert into alumno values
('1355894562',1,'Yinelly','Anchundia',5,'102','matematica'),
('1255824563',2,'Jordan','Delgado',9,'107','fisica'),
('1255823563',3,'Jose','Moreira',10,'106','lengua'),
('1455824563',4,'Adrian','Moreira',8,'102','Quimica'),
('1555814563',5,'Jordan','Delgado',7,'104','matematica');


insert into aula values
('ac34e','1234567892','1355894562',30,'matematica','Miguel Ponces',5),
('ag34e','1234567893','1255824563',30,'fisica','Darwin Zamora',9),
('ab34e','1234567894','1255823563',30,'lengua','Maria Delgado',10),
('ag24e','1234567895','1455824563',30,'Quimica','Mayerlin Delgado',8),
('ab44e','1234567896','1555814563',30,'matematica','Miguel Peres',7);



insert into docente values
('1234567892','Miguel','ponces','102','matematica',5),
('1234567893','Darwin','Zamora','107','fisica',9),
('1234567894','Maria','Delgado','106','lengua',10),
('1234567895','Mayerlin','Delgado','102','Quimica',8),
('1234567896','Miguel','Perez','104','matematica',7);




insert into materia values
(00002,'1234567892','1355894562','Miguel Ponces',30,'102',5),
(00003,'1234567893','1255824563','Darwin Zamora',30,'107',9),
(00004,'1234567894','1255823563','Maria Delgado',30,'106',10),
(00006,'1234567895','1455824563','Mayerlin Delgado',30,'102',8),
(00008,'1234567896','1555814563','Miguel Peres',30,'104',7);



insert into nivel values
(5,'matematica'),
(9,'fisica'),
(10,'lengua'),
(8,'Quimica'),
(7,'matematica');