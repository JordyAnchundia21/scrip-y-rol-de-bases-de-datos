/*==============================================================*/
/* CREACION DE USUARIOS Y ROLES                                 */
/*==============================================================*/



alter session set "_Oracle_SCRIPT"=true; 

CREATE USER userprofesor IDENTIFIED BY 123456;

GRANT CONNECT to userprofesor;

CREATE ROLE ControlInscripcion;

GRANT SELECT ANY TABLE,INSERT ANY TABLE,UPDATE ANY TABLE,DELETE ANY TABLE TO ControlInscripcion;

GRANT ControlInscripcion TO userprofesor;
