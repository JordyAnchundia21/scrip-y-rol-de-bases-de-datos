

/*==============================================================*/
/* Table: ALUMNO                                                */
/*==============================================================*/
create table ALUMNO (
   CEDULA               CHAR(10)             not null,
   NUMERO               INT                 null,
   NOMBRE               CHAR(30)             null,
   APELLIDO             CHAR(30)             null,
   NIVEL                INT                 null,
   AULA                 CHAR(30)             null,
   MATERIA              CHAR(30)             null,
   constraint PK_ALUMNO primary key (CEDULA)
);



/*==============================================================*/
/* Table: AULA                                                  */
/*==============================================================*/
create table AULA (
   CODIGO               CHAR(5)              not null,
   CEDULA               CHAR(10)             null,
   ALU_CEDULA           CHAR(10)             null,
   NRO_CUPOS            NUMERIC(30)          null,
   MATERIA              CHAR(30)             null,
   DOCENTE              CHAR(30)             null,
   NIVEL                INT                 null,
   constraint PK_AULA primary key (CODIGO)
);



/*==============================================================*/
/* Table: DOCENTE                                               */
/*==============================================================*/
create table DOCENTE (
   CEDULA               CHAR(10)             not null,
   NOMBRE               CHAR(30)             null,
   APELLIDO             CHAR(30)             null,
   AULA                 CHAR(3)              null,
   MATERIA              CHAR(30)             null,
   NIVEL                INT                 null,
   constraint PK_DOCENTE primary key (CEDULA)
);


/*==============================================================*/
/* Table: MATERIA                                               */
/*==============================================================*/
create table MATERIA (
   CODIGO               INT                 not null,
   CEDULA               CHAR(10)             null,
   ALU_CEDULA           CHAR(10)             null,
   DOCENTE              CHAR(30)             null,
   NRO_CUPOS            NUMERIC(30)          null,
   AULA                 CHAR(3)              null,
   NIVEL                INT                 null,
   constraint PK_MATERIA primary key (CODIGO)
);


/*==============================================================*/
/* Table: NIVEL                                                 */
/*==============================================================*/
create table NIVEL (
   NUMERO               INT                 not null,
   MATERIA              CHAR(30)             null,
   constraint PK_NIVEL primary key (NUMERO)
);
